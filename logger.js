// logger.js -- to produce feedback on various levels
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const config = require('config')
const logger = require('loglevel')
const prefix = require('loglevel-plugin-prefix')
const chalk = require('chalk')

const colors = {
  TRACE: chalk.magenta,
  DEBUG: chalk.cyan,
  INFO: chalk.blue,
  WARN: chalk.yellow,
  ERROR: chalk.red
}

prefix.reg(logger)

const level = config.loglevel || logger.levels.INFO
logger.setLevel(level)

prefix.apply(logger, {
  timestampFormatter: function (date) {
    return date.toISOString()
  },
  format (level, name, timestamp) {
    return `${chalk.gray(`[${timestamp}]`)} ${colors[level.toUpperCase()](level)}`
  }
})

module.exports = logger
