// parseUserInfo.js -- to collect salient details
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const logger = require('./logger')
const cheerio = require('cheerio')

module.exports = (body, user) => {
  const $ = cheerio.load(body)

  if ($('.error').text() === 'That user does not exist.') {
    logger.warn(user + ': account does not exist')
    return
  }

  const rv = {}
  rv.id = $('td', '#user-profile-personal-info').first().next()
    .text().match(/\d+/)[0]
  rv.user = $('span[property="sioc:name"]',
    '#user-profile-personal-info').text()
  rv.name = $('span[property="foaf:name"]',
    '#user-profile-personal-info').text()
  rv.mail = $('span[property="sioc:email_sha1"]',
    '#user-profile-personal-info').text()
    .replace(' @nospam@ ', '@')
  rv.since = $('td', '#user-profile-personal-info').last().text()
    .trim()
  return rv
}
