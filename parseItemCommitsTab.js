// parseItemCommitsTab.js
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const cheerio = require('cheerio')

module.exports = (tabBody) => {
  const $ = cheerio.load(tabBody)
  const rv = []
  if ($('td').text()) {
    console.error('Unexpected commit info:', $('td').text())
    rv.push($('td').text())
  }
  return rv
}
