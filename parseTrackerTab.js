// parseTrackerTab.js -- to extract project and tracker info
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const cheerio = require('cheerio')

module.exports = (body) => {
  const $ = cheerio.load(body)
  // FIXME extract project name
  const project = {
    name: $('h1', '#maindiv').first().text()
      .replace(/^Trackers for/, '').trim(),
    link: $('a[title="Project Homepage. Widgets oriented"]').attr('href')
  }

  const trackers = []
  $('tbody tr', '.listing').each(function (i, elem) {
    const col = $(this).children('td').first()
    const rv = {
      name: $(col).text().trim(),
      link: $(col).children('a').attr('href'),
      desc: $(col).next().text().trim()
    }
    const atid = rv.link.match(/atid=\d+/)[0].replace('atid=', '')
    rv.atid = atid
    trackers.push(rv)
  })
  project.trackers = trackers

  return project
}
