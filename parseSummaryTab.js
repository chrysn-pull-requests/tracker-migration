// parseSummaryTab.js -- to extract project information
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const cheerio = require('cheerio')

module.exports = (body) => {
  const $ = cheerio.load(body)

  const rv = {}
  rv.desc = $('.widget_content', '#widget_projectdescription-0').text()

  rv.tags = []
  $('.widget_content p', '#widget_projectinfo-0').first()
    .children('a').each(function (i, elem) {
      rv.tags.push($(this).text())
    })

  rv.trove = []
  $('.widget_content ul', '#widget_projectinfo-0').children('li')
    .each(function (i, elem) {
      rv.trove.push($(this).text())
    })

  rv.since = $('span[property="doap:created"]').text()

  return rv
}
